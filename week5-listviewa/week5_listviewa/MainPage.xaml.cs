﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace week5_listviewa
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            //var days = new List<string>{"Monday", "Tuesday", "Wednesday", "thursday", "Friday", "Saturday", "Sunday"};

            var daysWeather = new List<DaysWeather>
            {
                new DaysWeather {Day = "Monday", Weather = "Sunny", Image = "https://lorempixel.com/50/50/nature/1/" },
                new DaysWeather {Day = "Tuesday", Weather = "Rainy", Image = "https://lorempixel.com/50/50/nature/2/" },
                new DaysWeather {Day = "Wednesday", Weather = "Snow", Image = "https://lorempixel.com/50/50/nature/3/" },
                new DaysWeather {Day = "Thursday", Weather = "Showers", Image = "https://lorempixel.com/50/50/nature/4/" },
                new DaysWeather {Day = "Friday", Weather = "Cloudy", Image = "https://lorempixel.com/50/50/nature/5/" },
                new DaysWeather {Day = "Saturday", Weather = "Stormy", Image = "https://lorempixel.com/50/50/nature/6/" },
                new DaysWeather {Day = "Sunday", Weather = "Tornadoes", Image = "https://lorempixel.com/50/50/nature/7/"},
            };

            listView.ItemsSource = daysWeather;
        }
    }
}
