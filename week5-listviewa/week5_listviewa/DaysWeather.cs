﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week5_listviewa
{
    class DaysWeather
    {
        public string Day { get; set; }
        public string Weather { get; set; }
        public string Image { get; set; }
    }
}
